with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Exceptions ; use Ada.Exceptions ;
with cli ;

package body words.dealer is

   procedure ShowHint(ch : string) is
      hintchar : boolean := true ;
   begin
      for c in ch'range
      loop
         if hintchar
         then
            Put(ch(c));
            hintchar := false ;
         else
            Put('*');
            hintchar := true ;
         end if ;
      end loop ;
      New_Line ;
   end ShowHint ;
   
   procedure Play (cw : CandidateWords_Type) is
      Except_ID : Ada.Exceptions.EXCEPTION_OCCURRENCE;
   begin
      loop
         Put_Line("Let us start a new round");
         declare
            choice : string := Choose(cw) ;
            attempts : integer := 0 ;
         begin
            pragma Debug( Put_Line(choice) ) ;
            loop
               declare
                  guess : string := cli.Get("Your guess");
                  yourscore : integer := 0 ;
               begin
                  if guess = "quit"
                  then
                     Put("Your were to guess "); Put_Line(choice) ;
                     return ;
                  end if ;
                  if guess = "hint"
                  then
                     ShowHint(choice(1..cw.wordlength)) ;
                  else
                     if not IsCandidate(cw,guess)
                     then
                        Put(guess) ; Put_Line( " is not a candidate word") ;
                        exit ;
                     end if ;
                     attempts := attempts + 1 ;
                     yourscore := Score(choice,guess) ;
                     if yourscore = cw.wordlength
                       and choice(1..cw.wordlength) = guess
                     then
                        Put("Congratulations. You guessed it in ") ; Put(attempts); Put_Line(" attempts");
                        exit ;
                     end if ;
                     Put("That word scored "); Put(yourscore); Put_Line(". Try again");
                  end if ;
               exception
                  when others =>
                     Put(Ada.Exceptions.Exception_Name( Except_ID )) ;
                     Put_Line(Ada.Exceptions.Exception_Message( Except_ID ));
               end ;
            end loop ;
         end ;
      end loop ;
   end Play ;
   
end words.dealer;
