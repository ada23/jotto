with Ada.Containers.Vectors ;

package words is
   
   debug : boolean := false ;
   
   MAXLENGTH : constant Integer := 8 ;
   subtype Word_Type is string(1..MAXLENGTH) ;
   subtype MaxWordsType is integer range 1..100_000 ; 
   package Words_Pkg is new Ada.Containers.Vectors ( MaxWordsType, Word_Type ) ;
   type CandidateWords_Type is
      record
         wordlength : integer := 4 ;
         words : Words_Pkg.Vector ;
      end record ;
   
   function Initialize( wlen : integer := 4 ;
                        wordlist : string ) return CandidateWords_Type ;
   function Choose( cw : CandidateWords_Type ) return string ;
   function Choose( cw : CandidateWords_Type ) return integer ;
   
   function Score( toguess : string ; guess : string ) return integer ;
   function IsCandidate( cw : CandidateWords_Type; cand : string ) return boolean ;
   
end words;
