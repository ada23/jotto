with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

package cli is

   VERSION : string := "V01" ;
   NAME : String := "jotto" ;
   Verbose : aliased boolean ;

   PlayerOption : aliased boolean ;
   HelpOption : aliased boolean ;
   DebugOption : aliased boolean ;

   WordLength : aliased integer ;
   WordListFile : aliased GNAT.Strings.String_Access ;

   WordListFile1 : aliased String := "spellcheck-dict.txt" ;
   WordListFile2 : aliased String := "wordlist.txt" ;

   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowCommandLineArguments ;

   function Get(prompt : string) return String ;
end cli ;
