package revision is
   VER_MAJOR : constant :=           0 ;
   VER_MINOR : constant :=           0 ;
   VER_BUILD : constant :=          12 ;

   REPO : constant String := "git@gitlab.com:ada23/jotto.git" ;
   BRANCH : constant String := "master" ;
   COMMITID : constant String := "86a81fb030afe44e9cc2c8328af112906f1c314a" ;
   ABBREV_COMMITID : constant String := "86a81fb" ;

   VERSION : constant String := "0.0.12:master/86a81fb" ;
   BUILD_TIME : constant String := "2022-06-01 06:24:06" ;
end revision ;
