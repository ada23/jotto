with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Strings.Fixed ;
with GNAT.Random_Numbers ;

package body words is
      
   function Initialize( wlen : integer := 4 ;
                        wordlist : String ) return CandidateWords_Type is
      cw : CandidateWords_Type ;
      wfile : ada.text_Io.File_Type ;
      line : string(1..64) ;
      linelen : natural ;
      wordcount : integer := 0;
      newword : Word_Type ;
   begin
      cw.wordlength := wlen ;
      cw.words.Reserve_Capacity(10_000);
      
      -- Open(wfile,In_File,"../etc/wordlist.txt");
      Open(wfile,In_File,wordlist);
      while Not End_Of_File(wfile)
      loop
         get_line(wfile,line,linelen);
         if linelen = wlen
         then
            wordcount := wordcount + 1 ;
            Ada.Strings.Fixed.Move(Source => line(1..linelen) , Target => newword);
            cw.words.Append(newword) ;
            -- Put_Line(line(1..linelen));
         end if ;
      end loop ;
      Close(wfile) ;
      Put(wordcount); Put(" lines of word length "); Put(wlen); Put_Line(" read");
      Put("Storage vector length "); Put(Integer(cw.words.Length)) ; New_Line ;
      return cw ;
   end Initialize ;

   G : gnat.Random_Numbers.Generator ;
   
   function Choose( cw : CandidateWords_Type ) return string is
      idx : integer := Choose(cw) ;
   begin
      return cw.words.Element(idx) ;
   end Choose ;
   
   function Choose( cw : CandidateWords_Type ) return integer is
      numwords : float := float(cw.words.Length) ;
      idx : Integer ;
      fidx : Float := GNAT.Random_Numbers.Random(G) ;
   begin
      idx := Integer(fidx * numwords) ;
      --Put(idx); new_line ;
      if idx < 1
      then
         idx := 1 ;
      end if ;
      return idx ;
   end Choose ;
   
   function Score( toguess : string ; guess : string ) return integer is
      tgnorm : Word_Type ;
      gnorm : Word_Type ;
      result : integer := 0 ;
   begin
      Ada.Strings.Fixed.Move(toguess,tgnorm) ;
      Ada.Strings.Fixed.Move(guess,gnorm) ;
      if toguess = guess
      then
         return -1 ;
      end if ;
      --pragma Debug(Put_Line(tgnorm & gnorm));
      for c in gnorm'range
      loop
         if gnorm(c) /= ' '
         then
            for tc in tgnorm'range
            loop
               if tgnorm(tc) = ' '
               then
                  exit ;
               end if ;
               if gnorm(c) = tgnorm(tc)
               then
                  result := result + 1 ;
                  exit ;
               end if ;
            end loop ;
         end if ;
      end loop ;
      --Put("Score is " ); Put(result) ; New_Line ;
      return result ;
   end Score ;
   
   function IsCandidate( cw : CandidateWords_Type; cand : string ) return boolean is
      use Words_Pkg ;
      cursor : Words_Pkg.Cursor ;
      candword : Word_Type ;
   begin
      if cand'length /= cw.wordlength
      then
         return false ;
      end if ;
      ada.strings.fixed.move(cand,candword);
      cursor := cw.words.Find(candword) ;
      if cursor = Words_Pkg.No_Element
      then
         return false ;
      end if ;
      return true ;
   end IsCandidate ;
   
begin
   GNAT.Random_Numbers.Reset(G);
end words;
