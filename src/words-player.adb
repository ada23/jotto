with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with cli ;

package body words.player is

   type PackedBits_Type is array (integer range <>) of boolean ;
   pragma Pack(PackedBits_Type) ; 
   elmfile : File_Type ;
   procedure Inning( cw : CandidateWords_Type ) is
      
      wordavailable : PackedBits_Type( 1..Integer(cw.words.Length) ) := (others => true) ;
      idx : integer := Choose(cw) ;
      attempts : integer := 0 ;
      
      procedure EliminateWords( idx : integer ; score : integer ) is
         tempsc : integer ;
         eliminated : integer := 0 ;
         w1 : String := Words_Pkg.Element(cw.words,idx) ;
      begin
         for w in 1..cw.words.Last_Index
         loop
            if wordavailable(w)
            then
               declare
                  w2 : String := Words_Pkg.Element(cw.words,w) ;
               begin
                  tempsc := words.Score( w2 , w1 ) ;
                  if tempsc = score
                  then
                     null ;
                  else
                     if debug
                     then   
                        Put_Line(elmfile,w2);
                     end if ;
                     
                     wordavailable(w) := false ;
                     eliminated := eliminated + 1 ;
                  end if ; 
               end ;
            end if ;
         end loop ;
         Put("Eliminated "); Put(eliminated); Put_Line(" words");
         if debug
         then
            Put(elmfile,"Eliminated "); Put(elmfile, eliminated); Put_Line(elmfile , " words");
         end if ;
         end EliminateWords ;
      
   begin
      loop
         while not wordavailable(idx)
         loop
            idx := Choose(cw) ;
         end loop ;         
         attempts := attempts + 1 ;
         wordavailable(idx) := false ;
         Put("Attempt "); Put(attempts); New_Line ;
         Put("My Guess is "); Put(cw.words.Element(idx)); New_Line ;
         declare
            myscore : integer := Integer'Value( cli.Get("Score please [-1 if exact]") ) ;
         begin
            if myscore = -1
            then
               Put("That feels good. Only "); Put(attempts); Put_Line( " attempts");
               return ;
            end if ;
            if myscore > cw.wordlength
            then
               Put("Please score correctly. Word length is "); Put(cw.wordlength); New_Line;
            else
               EliminateWords( idx , myscore ) ;
            end if ;       
         end ;
      end loop ;
   end Inning ;
   
   procedure Play( cw : CandidateWords_Type ) is
      guessed : PackedBits_Type( 1 .. Integer(cw.words.Length) ) := (others => false);
   begin
      if debug
      then
         Create(elmfile,Out_File,"elmfile.txt");
      end if ;
      
      loop
         Inning (cw) ;
      end loop ;
   exception
      when others => 
         if debug
         then 
            Close(elmfile);
         end if ;
         raise ;
   end Play ;
   
end words.player;
