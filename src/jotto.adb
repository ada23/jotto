with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_IO; use Ada.Integer_Text_Io;
with GNAT.Command_Line ;

with cli ;
with words ;
with words.dealer ;
with words.player ;

procedure Jotto is
   cw : words.CandidateWords_Type ;

   procedure Choose_Tests is
   begin
      for i in 1..10
      loop
         Put_Line(words.Choose(cw));
      end loop;
   end Choose_Tests ;

   procedure Score_Tests is
      procedure Score_Test(w1 : string; w2: string) is
      begin
         Put("Word1 "); Put(w1);
         Put(" Word2 "); Put(w2);
         Put(" Score "); Put( words.Score(w1,w2) ); New_Line ;
      end Score_Test ;
   begin
      Score_Test("armor" , "ardor");
      Score_Test("troop" , "prime");
      Score_Test("compute","temper");
   end Score_Tests ;
   procedure Candidate_Tests is
      procedure Candidate_Test( cand : string ) is
      begin
         Put(cand) ; Put(" ") ;
         if words.IsCandidate(cw,cand)
         then
            Put_Line("Is a candidate");
         else
            Put_Line("Is not a candidate");
         end if ;
      end Candidate_Test ;
   begin
      Candidate_Test("graphic");
      Candidate_Test("linger");
      Candidate_Test("piglet");
      Candidate_Test("aaaaaa");
      Candidate_Test("sweets");
   end Candidate_Tests ;
   procedure Dealer_Test is
   begin
      words.dealer.Play(cw);
   end Dealer_Test ;
   procedure Player_Test is
   begin
      words.player.Play(cw);
   end Player_Test ;

begin
   cli.ProcessCommandLine ;
   cw := words.Initialize(cli.WordLength, cli.WordListFile.all );
   if cli.PlayerOption
   then
      words.Player.Play(cw) ;
   else
      words.Dealer.Play(cw) ;
   end if ;
exception
   when GNAT.Command_Line.Exit_From_Command_Line =>
      return ;
end Jotto;
