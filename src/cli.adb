with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with revision ; 
with GNAT.Command_Line;
with GNAT.Source_Info; use GNAT.Source_Info;

package body cli is

   package boolean_text_io is new Enumeration_IO (Boolean);
   use boolean_text_io;

   procedure SwitchHandler
     (Switch : String; Parameter : String; Section : String)
   is
   begin
      Put ("SwitchHandler " & Switch);
      Put (" Parameter " & Parameter);
      Put (" Section " & Section);
      New_Line;
   end SwitchHandler;

   procedure ProcessCommandLine is
      Config : GNAT.Command_Line.Command_Line_Configuration;
   begin

      GNAT.Command_Line.Set_Usage
        (Config,
         Help =>
           NAME & " " & revision.VERSION & " " & Compilation_ISO_Date & " " &
           Compilation_Time,
         Usage => "guess");

      GNAT.Command_Line.Define_Switch
        (Config, Verbose'access, Switch => "-v", Long_Switch => "--verbose",
         Help                           => "Output extra verbose information");

      GNAT.Command_Line.Define_Switch
        (Config, DebugOption'access, Switch => "-d", Long_Switch => "--debug",
         Help                           => "Save debug logging info");

      GNAT.Command_Line.Define_Switch
        (Config, PlayerOption'access, Switch => "-p",
         Long_Switch => "--player", Help => "Simple player. Let human be the guesser");

      GNAT.Command_Line.Define_Switch
        (Config, WordLength'access, Switch => "-w=",
         Long_Switch => "--word-length=", Help => "Word Length 4-8",
         Initial => 4 ,
         Default => 4 );

      GNAT.Command_Line.Define_Switch
        (Config, WordListFile'access, Switch => "-f=",
         Long_Switch => "--word-list-file-name=", Help => "Word list file name");


      GNAT.Command_Line.Getopt (Config, SwitchHandler'access);
      if WordListFile.all'Length < 1
      then
         WordListFile := WordListFile1'Access ;
      end if ;

      if Verbose
      then
         ShowCommandLineArguments ;
      end if ;

   end ProcessCommandLine;

   function GetNextArgument return String is
   begin
      return GNAT.Command_Line.Get_Argument (Do_Expansion => True);
   end GetNextArgument;

   procedure ShowCommandLineArguments is
   begin
      Put ("Verbose ");
      Put (Verbose);
      New_Line;

      Put ("Player Option ");
      Put (PlayerOption);
      New_Line;

      Put("Word list Filename ");
      Put(WordListFile.all);
      New_Line ;

      Put("Word Length ");
      Put(WordLength);
      New_Line ;

   end ShowCommandLineArguments;

   function Get(Prompt : string) return string is
      result : String(1..80);
      len : natural ;
   begin
      Ada.Text_Io.Put(Prompt) ; Ada.Text_Io.Put(" > "); Ada.Text_Io.Flush ;
      Ada.Text_Io.Get_Line(result,len);
      return result(1..len);
   end Get ;

end cli;
